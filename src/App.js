import React from 'react';
import './App.css';

const accurateInterval = function (fn, time) {
  var cancel, nextAt, timeout, wrapper;
  nextAt = new Date().getTime() + time;
  timeout = null;
  wrapper = function () {
    nextAt += time;
    timeout = setTimeout(wrapper, nextAt - new Date().getTime());
    return fn();
  };
  cancel = function () {
    return clearTimeout(timeout);
  };
  timeout = setTimeout(wrapper, nextAt - new Date().getTime());
  return {
    cancel: cancel,
  };
};

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      breakLength: 5,
      sessionLength: 25,
      timerState: 'stopped',
      timerType: 'Session',
      timer: 1500,
      intervalID: '',
    };
  }

  setBrkLength(sign) {
    this.lengthControl('breakLength', sign, this.state.breakLength, 'Session');
  }

  setSeshLength(sign) {
    this.lengthControl(
      'sessionLength',
      sign,
      this.state.sessionLength,
      'Break'
    );
  }

  lengthControl(stateToChange, sign, currentLength, timerType) {
    if (this.state.timerState === 'running') return;
    if (this.state.timerType === timerType) {
      if (sign === '-' && currentLength !== 1) {
        this.setState({ [stateToChange]: currentLength - 1 });
      } else if (sign === '+' && currentLength !== 60) {
        this.setState({ [stateToChange]: currentLength + 1 });
      }
    } else {
      if (sign === '-' && currentLength !== 1) {
        this.setState({
          [stateToChange]: currentLength - 1,
          timer: currentLength * 60 - 60,
        });
      } else if (sign === '+' && currentLength !== 60) {
        this.setState({
          [stateToChange]: currentLength + 1,
          timer: currentLength * 60 + 60,
        });
      }
    }
  }

  reset() {
    this.setState({
      breakLength: 5,
      sessionLength: 25,
      timerState: 'stopped',
      timerType: 'Session',
      timer: 1500,
      intervalID: '',
    });
    this.state.intervalID && this.state.intervalID.cancel();
    this.audioBeep.pause();
    this.audioBeep.currentTime = 0;
  }

  parseTime() {
    let minutes = Math.floor(this.state.timer / 60);
    let seconds = this.state.timer - minutes * 60;

    seconds = seconds < 10 ? '0' + seconds : seconds;
    minutes = minutes < 10 ? '0' + minutes : minutes;

    return minutes + ':' + seconds;
  }

  timerControl() {
    if (this.state.timerState === 'stopped') {
      this.beginCountDown();
      this.setState({ timerState: 'running' });
    } else {
      this.setState({ timerState: 'stopped' });
      this.state.intervalID && this.state.intervalID.cancel();
    }
  }

  beginCountDown() {
    this.setState({
      intervalID: accurateInterval(() => {
        this.setState({ timer: this.state.timer - 1 });
        this.phaseControl();
      }, 1000),
    });
  }

  phaseControl() {
    let timer = this.state.timer;
    this.buzzer(timer);
    if (timer < 0) {
      if (this.state.timerType === 'Session') {
        this.state.intervalID && this.state.intervalID.cancel();
        this.beginCountDown();
        this.switchTimer(this.state.breakLength * 60, 'Break');
      } else {
        this.state.intervalID && this.state.intervalID.cancel();
        this.beginCountDown();
        this.switchTimer(this.state.sessionLength * 60, 'Session');
      }
    }
  }

  switchTimer(num, str) {
    this.setState({
      timer: num,
      timerType: str,
    });
  }

  buzzer(_timer) {
    if (_timer === 0) {
      this.audioBeep.play();
    }
  }

  render() {
    return (
      <div className="container">
        <h1>Pomodoro Clock</h1>
        <div className="settings">
          <div className="settings__panel">
            <div className="settings__title" id="break-label">
              Break Length
            </div>
            <div className="settings__active">
              <button
                className="settings__button"
                id="break-decrement"
                onClick={() => this.setBrkLength('-')}
              >
                <i className="fas fa-chevron-left"></i>
              </button>
              <div className="settings__value" id="break-length">
                {this.state.breakLength}
              </div>
              <button
                className="settings__button"
                id="break-increment"
                onClick={() => this.setBrkLength('+')}
              >
                <i className="fas fa-chevron-right"></i>
              </button>
            </div>
          </div>
          <div className="right-settings__panel">
            <div className="settings__title" id="session-label">
              Session Length
            </div>
            <div className="settings__active">
              <button
                className="settings__button"
                id="session-decrement"
                onClick={() => this.setSeshLength('-')}
              >
                <i className="fas fa-chevron-left"></i>
              </button>
              <div className="settings__value" id="session-length">
                {this.state.sessionLength}
              </div>
              <button
                className="settings__button"
                id="session-increment"
                onClick={() => this.setSeshLength('+')}
              >
                <i className="fas fa-chevron-right"></i>
              </button>
            </div>
          </div>
        </div>
        <div className="timer">
          <div className="timer__title" id="timer-label">
            {this.state.timerType}
          </div>
          <div className="timer__value" id="time-left">
            {this.parseTime()}
          </div>
        </div>
        <div className="control-panel">
          <button
            className="control-panel__button"
            id="start_stop"
            onClick={() => this.timerControl()}
          >
            <i className="fas fa-play"></i>
            <i className="fas fa-stop"></i>
          </button>
          <button
            className="control-panel__button"
            id="reset"
            onClick={() => this.reset()}
          >
            <i className="fas fa-sync-alt"></i>
          </button>
        </div>
        <audio
          id="beep"
          preload="auto"
          src="BeepSound.wav"
          ref={(audio) => {
            this.audioBeep = audio;
          }}
        />
      </div>
    );
  }
}

export default App;
